package com.unlid.driver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.unlid.model.Sentence;
import com.unlid.util.FileOperations;
import com.unlid.util.UNLIDConstants;

public class UNLIDDriver {

	public static void main(String[] args) throws IOException {

		/** All matched sentences will be saved in this list */
		List<String> matchedSentences = new ArrayList<String>();
		
		while (true) {

			System.out.println("List of files in the selected directory");
			// TODO Select file to work on
			String fileName = getFileNameToWorkOn();

			// TODO Read query from the console
			List<String> query = readInputFromConsole();

			// TODO Read sentence from file
			BufferedReader fileReader = null;
			String line = null;

			// TODO Get File Reader
			fileReader = FileOperations
					.getFileReader(UNLIDConstants.DATA_DIRECTORY
							+ File.separator + fileName);
			line = fileReader.readLine(); // Skipping first sentence marker

			while (line != null) {
				// TODO Populating Sentence Object
				Sentence sentence = new Sentence();
				
				if (line.equalsIgnoreCase(UNLIDConstants.FILE_END_MARKER))
					break;
				line = populatingSentenceObject(fileReader, sentence);

				// TODO Validating query parts exit in sentence
				if (isSentenceQualifiedForProcessing(query, sentence)) {

					// TODO Creating RegEx to find sentence
					String regex = createRegEx(query, sentence);
					
					// TODO Matching the strings using RegEx
					matchSentencesBasedOnRegEx(matchedSentences, sentence,
							regex);
				}// end if else
			}// end while loop

			// TODO Printing the filtered sentences
			printMatchedSentences(matchedSentences);

			// TODO Ask user to terminate program
			if (!FileOperations.getUserChoice()) {
				fileReader.close();
				System.out.println("!!! Good Bye !!!");
				System.exit(0);
			}
			// out.close();
		}// end outer while
	}

	public static String createRegEx(List<String> query, Sentence sentence) {
		List<String> regexElements1 = new ArrayList<String>();
		for (String queryLine : query) {
			String queryLabel = queryLine.split(":")[0].trim();
			String queryValue = queryLine.split(":")[1].trim();

			if (queryLabel.equalsIgnoreCase("token")) {
				regexElements1.add(sentence.getTokenMap().get(
						queryValue.toUpperCase()));
			} else if (queryLabel.equalsIgnoreCase("pos")) {
				regexElements1.add("("
						+ String.join(
								"|",
								sentence.getPosMap().get(
										queryValue.toUpperCase())) + ")");
			} else if (queryLabel.equalsIgnoreCase("sem")) {
				regexElements1.add("("
						+ String.join(
								"|",
								sentence.getSemMap().get(
										queryValue.toUpperCase())) + ")");
			}
		}// end for loop
		List<String> regexElements = regexElements1;
		String regex = String.join("\\s", regexElements);
		return regex;
	}

	public static void printMatchedSentences(List<String> matchedSentences) {
		if (matchedSentences.size() > 0) {
			System.out.println("---------------" + matchedSentences.size()
					+ " MATCH FOUND--------------");
			for (String s : matchedSentences) {
				System.out.println("- " + s);
			}
		} else {
			System.err.println("No Match Found");
		}
	}

	public static void matchSentencesBasedOnRegEx(
			List<String> matchedSentences, Sentence sentence, String regex) {
		String orgSentence = sentence.getSentence().toUpperCase();
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(orgSentence);

		if (matcher.find()) {
			// System.out.println("MATCH FOUND");
			// System.out.println("Found value: " + matcher.group(0));
			// TODO Final Matched Sentences List
			matchedSentences.add(sentence.getSentence());
		} else {
			// System.out.println("NO MATCH");
		}
	}

	public static List<String> formattingDataForRegex(List<String> query,
			Sentence sentence) {
		List<String> regexElements = new ArrayList<String>();
		for (String queryLine : query) {
			String queryLabel = queryLine.split(":")[0].trim();
			String queryValue = queryLine.split(":")[1].trim();

			if (queryLabel.equalsIgnoreCase("token")) {
				regexElements.add(sentence.getTokenMap().get(
						queryValue.toUpperCase()));
			} else if (queryLabel.equalsIgnoreCase("pos")) {
				regexElements.add("("
						+ String.join(
								"|",
								sentence.getPosMap().get(
										queryValue.toUpperCase())) + ")");
			} else if (queryLabel.equalsIgnoreCase("sem")) {
				regexElements.add("("
						+ String.join(
								"|",
								sentence.getSemMap().get(
										queryValue.toUpperCase())) + ")");
			}
		}// end for loop
		return regexElements;
	}

	public static String getFileNameToWorkOn() {
		List<String> filesInDirectory = FileOperations
				.readDirectory(UNLIDConstants.DATA_DIRECTORY);
		for (int fileIndex = 0; fileIndex < filesInDirectory.size(); fileIndex++) {
			System.out.println(fileIndex + 1 + " - "
					+ filesInDirectory.get(fileIndex));
		}

		Scanner in = new Scanner(System.in);
		System.out.print("Enter file number : ");
		String fileName = filesInDirectory
				.get(Integer.parseInt(in.nextLine()) - 1);
		System.out.println("Your selected file is : " + fileName);
		// in.close();
		return fileName;
	}

	public static boolean isSentenceQualifiedForProcessing(List<String> query,
			Sentence sentence) {

		for (String queryLine : query) {
			String queryLabel = queryLine.split(":")[0].trim();
			String queryValue = queryLine.split(":")[1].trim();

			// Check the whole query is valid
			if (!isQueryValueExists(sentence, queryLabel, queryValue)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isQueryValueExists(Sentence sentence,
			String queryLabel, String queryValue) {

		if (queryLabel.equalsIgnoreCase("token"))
			return sentence.getTokenMap().containsKey(queryValue.toUpperCase());
		else if (queryLabel.equalsIgnoreCase("pos"))
			return sentence.getPosMap().containsKey(queryValue.toUpperCase());
		else if (queryLabel.equalsIgnoreCase("sem"))
			return sentence.getSemMap().containsKey(queryValue.toUpperCase());

		return false;
	}

	public static String populatingSentenceObject(BufferedReader fileReader,
			Sentence sentence) throws IOException {
		String line;
		line = fileReader.readLine();
		while (!line.equalsIgnoreCase(UNLIDConstants.SENTENCE_MARKER)) {
			String[] lineTokens = line.split("	");
			String token = lineTokens[0];
			String pos = lineTokens[1];
			String sem = lineTokens[2];

			// TODO Populating the object
			sentence.addToken(token);
			sentence.addPOS(pos, token);
			sentence.addSem(sem, token);
			sentence.makeSentence(token);

			// TODO Reading next line from file
			line = fileReader.readLine();
			if (line.equalsIgnoreCase(UNLIDConstants.FILE_END_MARKER))
				break;
		}
		//System.out.println(sentence.toString());
		//System.exit(0);
		return line;
	}

	public static ArrayList<String> readInputFromConsole() {

		BufferedReader br = null;
		ArrayList<String> query = new ArrayList<String>();

		System.out
				.println("Write your query as per defined format i.e. Label: Value.");
		System.out.println("Press 'ENTER' to move on next line");
		System.out.println("-----------Example Query-----------");
		System.out.println("POS  : PR");
		System.out.println("POS  : DT");
		System.out.println("TOKEN: the");
		System.out.println("SEM  : Time");
		System.out
				.println("Once you are finished with the query writing, write 'exit' to take exit from console.");
		System.out.println(".....Click below to start query writing...");

		try {
			br = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				String input = br.readLine();

				if ("exit".equalsIgnoreCase(input)) {
					// System.out.println("Exit! You are now out of console.");
					// System.exit(0);
					break;
				}
				if (input != null && input.length() > 0)
					query.add(input);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return query;
	}
}
