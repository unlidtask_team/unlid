package com.unlid.util;

import static java.nio.file.Paths.get;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class FileOperations {
	/**
	 * Read each file in the given directory and put this file in ArrayList. At
	 * each element of list, there will be a token of each file.
	 * 
	 * @param direcotryPath
	 *            Path of the directory which contains text files.
	 * @param fileReadingFormat
	 *            Format in which user want to read text e.g.
	 *            StandardCharsets.UTF_8
	 * @return ArrayList of text. Each element of this list contains whole
	 *         single line of each file in the directory.
	 * 
	 */
	public static ArrayList<String> directoryToList(String direcotryPath,
			Charset fileReadingFormat) {
		ArrayList<String> allFilesText = new ArrayList<String>();

		File filesDirectory = new File(direcotryPath);
		File[] listOfAllTextFiles = filesDirectory.listFiles();
		if (listOfAllTextFiles.length > 0) {
			for (int i = 0; i < listOfAllTextFiles.length; i++) {
				List<String> docTextLines;
				try {
					docTextLines = Files.readAllLines(
							get(direcotryPath + File.separator
									+ listOfAllTextFiles[i].getName()),
							fileReadingFormat);
					// String joinedText = String.join(" ", docTextLines);
					allFilesText.addAll(docTextLines);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		return allFilesText;
	}

	public static void writeTextToFile(String filePath, String text)
			throws IOException {
		File file = new File(filePath);
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(text);
		bw.close();
	}

	public static void writeListToTextFile(String filePath,
			ArrayList<String> stringList) throws IOException {
		File file = new File(filePath);

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		String fileString = "";
		PrintWriter pw = new PrintWriter(new File(filePath), "UTF-8");

		for (String token : stringList)
			fileString = fileString + token.trim() + "\n";
		System.out.println(fileString.trim());
		pw.println(fileString);
		pw.close();

	}

	/**
	 * Reads the given text file and convert it to arraylist. Each line of the
	 * file will be placed on each element of the arraylist.
	 * 
	 * @param filePath
	 *            Complete path of the text file including name and extenssion
	 * @param fileReadingFormat
	 *            formate of the file e.g. StandardCharset.UTF_8
	 * @return
	 * @throws IOException
	 */
	public static ArrayList<String> fileToArrayList(String filePath,
			Charset fileReadingFormat) throws IOException {
		ArrayList<String> docTextLines = (ArrayList<String>) Files
				.readAllLines(get(filePath), fileReadingFormat);
		return docTextLines;
	}

	public static BufferedReader getFileReader(String file) {
		BufferedReader br = null;
		//FileReader fr = null;

		try {

			//fr = new FileReader(file);
			br = new BufferedReader(new FileReader(file));

			// TODO Skipping head lines of the source text file
			br.readLine(); // Skipping 1st line
			br.readLine(); // Skipping 2nd line
			

		} catch (IOException e) {
			e.printStackTrace();
		}

		return br;
	}
	
	public static boolean getUserChoice() {
		BufferedReader br = null;
		//FileReader fr = null;

		try {

			br = new BufferedReader(new InputStreamReader(System.in));

			System.out.print("Do you want to continue (Y | N) : ");
			// TODO Skipping head lines of the source text file
			String choice = br.readLine();
			if(choice.equalsIgnoreCase("n"))
				return false;			
		} catch (IOException e) {
			e.printStackTrace();
		} 

		return true;
	}

	public static List<String> readDirectory(String dirPath) {
		List<String> filesInDirectory = null;
		File filesDirectory = new File(dirPath);
		File[] listOfAllTextFiles = filesDirectory.listFiles();
		if (listOfAllTextFiles.length > 0) {
			filesInDirectory = new ArrayList<String>();
			for (int i = 0; i < listOfAllTextFiles.length; i++) {
				filesInDirectory.add(listOfAllTextFiles[i].getName());
			}
		}
		
		return filesInDirectory;
	}
}
