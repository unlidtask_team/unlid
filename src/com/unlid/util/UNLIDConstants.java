package com.unlid.util;

import java.util.ArrayList;

public class UNLIDConstants {

	public static final ArrayList<String> PREPOSITIONS = new ArrayList<String>();
	public static final ArrayList<String> DETERMINER = new ArrayList<String>();
	
	public static final String DEFAULT_OUTPUT_DIR =".\\Output";
	public static final String DEFAULT_INPUT_DIR =".\\input";
	public static final String DATA_DIRECTORY =".\\input\\data";
	
	public static final String SENTENCE_MARKER ="-----	-----	\"\"	-----	CD";
	public static final String FILE_END_MARKER ="\"</text>";
	
	
	public static void loadPOSC7TagSet() {
		//Prepositions	    
	    PREPOSITIONS.add("IF");
	    PREPOSITIONS.add("II");
	    PREPOSITIONS.add("IO");
	    PREPOSITIONS.add("IW");
	    
	  //Determiners
	    DETERMINER.add("DA");
	    DETERMINER.add("DA1");
	    DETERMINER.add("DA2");
	    DETERMINER.add("DAR");
	    DETERMINER.add("DAT");	    
	    DETERMINER.add("DB");
	    DETERMINER.add("DB2");
	    DETERMINER.add("DD");
	    DETERMINER.add("DD1");
	    DETERMINER.add("DD2");	    
	    DETERMINER.add("DDQ");
	    DETERMINER.add("DDQGE");
	    DETERMINER.add("DDQV");
	    
	    
	}
}
