package com.unlid.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;



public class Sentence {

	private Map<String, String> tokenMap = new ConcurrentHashMap<String,String>();
	private Map<String, List<String>> posMap = new ConcurrentHashMap<String, List<String>>();
	private Map<String, List<String>> semMap = new ConcurrentHashMap<String, List<String>>();
	private List<String> sentence = new ArrayList<String>(); 
	
	
	
	
	/**
	 * @return the tokenMap
	 */
	public Map<String, String> getTokenMap() {
		return tokenMap;
	}

	/**
	 * @return the posMap
	 */
	public Map<String, List<String>> getPosMap() {
		return posMap;
	}

	/**
	 * @return the semMap
	 */
	public Map<String, List<String>> getSemMap() {
		return semMap;
	}

	public Sentence(){
		
	}
	
	/**
	 * @param tokenMap
	 * @param posMap
	 * @param semMap
	 */
	public Sentence(String token, String pos, String sem) {
		this.addToken(token);
		this.addPOS(pos, token);
		this.addSem(sem, token);
		this.makeSentence(token);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Sentence [tokenMap=" + tokenMap + ", posMap=" + posMap
				+ ", semMap=" + semMap + ", sentence=" + sentence + "]";
	}

	public void makeSentence(String token){
		sentence.add(token);
	}
	
	public String getSentence(){
		return String.join(" ", sentence);
	}
	
	public void addToken(String token){
		tokenMap.put(token.toUpperCase(), token.toUpperCase());
	} 
	
	public void addPOS(String pos, String token){
		List<String> posList = posMap.get(pos);
		if(posList==null){
			posList = new ArrayList<String>();
		}
		posList.add(token.toUpperCase());
		posMap.put(pos.toUpperCase(), posList);		
	}
	
	public void addSem(String sem, String token){
		List<String> semList = semMap.get(sem);
		if(semList==null){
			semList = new ArrayList<String>();
		}
		
		String[] semArray = sem.split("\\s|/");
		semList.add(token.toUpperCase());
		
		if(semArray.length>0){
			for(int i=0;i<semArray.length;i++){
				//semList.add(token.toUpperCase());
				semMap.put(semArray[i].toUpperCase(), semList);
			}
			
			/*for(String semString : semArray ){
				semList.add(token.toUpperCase());
				semMap.put(semString.toUpperCase(), semList);
			}*/
		}
		
		//semList.add(token.toUpperCase());
		//semMap.put(sem.toUpperCase(), semList);
	}
}
